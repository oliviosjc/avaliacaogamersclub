import { Component, OnInit, Input} from '@angular/core';
import { ApiModel } from '../../models/api.model';
declare var $: any;

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  queuePremiumQt: number;

  constructor() { }

  @Input() lista: ApiModel;

  ngOnInit() 
  {
    this.getQueuePremiumQuantity();
  }

  getQueuePremiumQuantity()
  {
   this.lista.games.forEach(g => {
     if(g.title == "Ranked Open")
     {
     this.queuePremiumQt = g.cta.line; 
     }
   })
  }
}
