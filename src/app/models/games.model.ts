import { CtaModel } from "./cta.model";

export class GamesModel {
    title: string;
    cta: CtaModel;
    image: string;
    matches: number;
    win: number;
    lose: number;
    constructor() 
    {
        this.cta = new CtaModel();
    }
}