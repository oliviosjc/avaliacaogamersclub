export class GamePositionModel {
    id: number;
    title: string;
    image: string;
}