import { UserModel } from '../models/user.model';
import { FourFunModel } from '../models/fourfun.model';
import { GamesModel } from '../models/games.model';

export class ApiModel {
    online: string;
    latest_banned: number;
    user: UserModel[];
    fourFun: FourFunModel[];
    games: GamesModel[];

    constructor() 
    {
        this.user = [];
        this.fourFun = [];
        this.games = [];
    }
}