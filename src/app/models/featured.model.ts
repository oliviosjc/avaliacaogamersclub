export class FeaturedModel {
    id: number;
    title: string;
    image: string;
}