import { GamePositionModel } from '../models/gamePosition.model';
import { FeaturedModel } from '../models/featured.model';

export class UserModel {
    name: string;
    id: number;
    expertise: string;
    level: number;
    is_subscriber: boolean;
    patent: string;
    game_position: GamePositionModel;
    featured_medal: FeaturedModel;

    constructor() 
    {
        this.game_position = new GamePositionModel();
        this.featured_medal = new FeaturedModel();
    }
}