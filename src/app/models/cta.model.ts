export class CtaModel {
    title: string;
    link: string;
    line: number;
}