import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiModel } from '../models/api.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  returnApiModel: ApiModel;

  constructor(private http: HttpClient) { }

  public async getApi(): Promise<ApiModel> {

    let request = await new Promise<ApiModel>((resolve) => 
    {
      this.http.get<ApiModel>('https://api.myjson.com/bins/sqwaq').subscribe(s => {
        resolve(s);
      });
    });

   return request;
    
  }
}