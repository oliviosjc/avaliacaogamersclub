import { Component, OnInit } from '@angular/core';
import { UserService } from '../app/services/user.service';
import { ApiModel } from './models/api.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{

  title = 'GamersClub';
  data : ApiModel;
  constructor(private userService: UserService) { }
  
  ngOnInit() 
  {
    this.getData();
  }

  getData()
  {
    this.userService.getApi().then(t => {
      //convert object start's with 4 to four. 
      let remove4fun = JSON.stringify(t);
      remove4fun = remove4fun.replace("4fun","fourFun");
      this.data = JSON.parse(remove4fun);
    });
  }
}
